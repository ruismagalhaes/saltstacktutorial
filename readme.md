# Tutorial about SaltStack

## SaltStack Test environment with docker

On this section, an explanation will be given on how to create a test environment with docker and docker-compose for saltstack. 
With this solution, you don't need to install the tools and its dependencies directly on your machine or on a cloud provider neither use software to build virtual machines environments. 
You only need docker, and the files on this repository.

### how-to

Just run the following:
```
docker-compose up
```

Now, you can see all the logs from salt-master and salt-minion.

**Next, we will see some useful commands that can be used.**

### Running Multiple Minions

To run 2 minions:

```
docker-compose up --scale salt-minion=2
```

**NOTE:** The hostnames of the minions in this setup are their docker container IDs: salt-master and salt-minion.
If you start up with a scale setting of more than one, the actual names of the minions will be docker-saltstack_salt-minion_1 and docker-saltstack_salt-minion_2.

### Logs into salt-master or salt-minion containers

**Salt-master:**

```
docker-compose exec salt-master bash
```

**Salt-minion:**

If the number of minions is only 1:

```
docker-compose exec salt-master bash
```

If the number of minions is more than one:

```
docker-compose exec --index=2 salt-minion bash
```

where **index** is the index of the minion that you want to log

### Starting/Stopping/Restarting

You can "ctrl-c" to stop the docker stack in your log monitoring window, that will shut down all the containers.

To restart all the minions (useful if you edit the /etc/salt/minion file for example):
```
docker-compose restart salt-minion
```

If you want to run the cluster in the background without watching the logs:
```
docker-compose up -d
```

To stop the cluster:
```
docker-compose down
```

To adjust the scale of the minions (which you can do while its running):
```
docker-compose up -d --scale salt-minion=3
```
Setting a scale less than the current scale will shut down containers.

## Testing the set-up

Now that we have a container running the salt-master and, at least 1 minion running on another container,
we can test if everything is running as expected.

Log in the salt-master:
```
docker-compose exec salt-master bash
```

Ping the minions:
```
salt '*' test.ping
```
All the minions should return "true"

You can query the grains on the minions to find out more about them:
```
salt '*' grains.get os
```
This command reports back the operating system of each minion (On this case, Ubuntu)

Copy a file to all the minions:
```
touch myfile.txt
salt-cp '*' myfile.txt /myfile.txt
```
Now, any minion should have the "myfile.txt"

## Useful Modules

### Common Execution modules

In this section you can see a handful of common execution modules.

1) Used to extract the documentation of the function "ping" on the module "test" from the "jerry's" minion:
```
salt jerry sys.doc test.ping
```

2) Used to extract all the documentation from the module "test" from the "jerry's minion:
```
salt jerry sys.doc test
```

3) List all the modules from "jerry's" minion:
```
salt jerry sys.list_modules
```

4) List all the functions of all modules from "jerry's" minion:
```
salt jerry sys.list_functions
```

5) List all the packages installed on "jerry's" minion:
```
salt jerry pkg.list_pkgs
```

6) Install "wget" package on "jerry's" minion:
```
salt jerry pkg.install wget
```

7) List all the user's that are available on "stuart's" minion:
```
salt stuart user.list_users
```

8) Return a list of all info for all users available on "stuart's" minion:
```
salt stuart user.getent 
```

9) get uptime of "stuart's" minion:
```
salt stuart status.uptime
```

10) get disk space info of "stuart's" and "jerry's" minions:
```
salt -L stuart,jerry status.diskusage
```

11) Run CLI commands on "stuart's" and "jerry's" minions:
```
salt -L stuart,jerry cmd.run 'cat /etc/salt/grains'
salt -L stuart,jerry cmd.run 'apt-get update && apt-get install -y curl'
salt -L stuart,jerry cmd.run 'curl https://google.com'
```

12) get all grains of "stuart's" and "jerry's" minions:
```
salt -L stuart,jerry grains.items
```

13) get OS family of "stuart's" and "jerry's" minions:
```
salt -L stuart,jerry grains.get os_family
```

14) list files that "stuart's" and "jerry's" minions can see:
```
salt -L stuart,jerry cp.list_master
```

## cleaning up environments

Never forget to clear your containers and images after you are finished with this tutorial:
**NOTE:** Be careful using this commands. This will delete all your images, containers and volumes from your machine.

```
docker-compose down
docker system prune
docker volume prune
docker rmi $(docker images -q)
```

